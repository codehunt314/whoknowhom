from django.db import models


class Bucket(models.Model):
    name        =    models.CharField(max_length=255)
    keywords    =    models.TextField()
    status      =    models.CharField(max_length=255, blank=True, null=True)


class TwitterProfile(models.Model):
    bucket          =   models.ForeignKey(Bucket)
    userid          =   models.CharField(max_length=255)
    username        =   models.CharField(max_length=255)
    userdetail      =   models.TextField(blank=True, null=True)
    userfriends     =   models.TextField(blank=True, null=True)
    userfollowers   =   models.TextField(blank=True, null=True)
    seedprofile     =   models.BooleanField(default=False)
    explored        =   models.BooleanField(default=False)


class TwitterProfileQueue(models.Model):
    bucket          =   models.ForeignKey(Bucket)
    userid          =   models.CharField(max_length=255)
    username        =   models.CharField(max_length=255)
    userdetail      =   models.TextField(blank=True, null=True)
    userfriends     =   models.TextField(blank=True, null=True)
    userfollowers   =   models.TextField(blank=True, null=True)
    seedprofile     =   models.BooleanField(default=False)
    explored        =   models.BooleanField(default=False)



"""
def rule_followedback():
    pass
#Twitter Relation Map
class UserRelation(models.Model):
    usera   = models.CharField(max_length=255)
    userb   = models.CharField(max_length=255)
    atob    = models.BooleanField(default=False)
    btoa    = models.BooleanField(default=False)
    rdetail  = models.TextField(blank=True, null=True)

class UserDetail(models.Model):
    uid     = models.CharField(max_length=255)
    udetail = models.TextField(blank=True, null=True)
"""
