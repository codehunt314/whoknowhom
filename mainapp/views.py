from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
import re, random, httplib, urllib, json, time
import oauth2 as oauth
from django.core.paginator import Paginator
from datetime import datetime

from mainapp.models import *

from pymongo import Connection
connection  =   Connection()
mongo_db    =   connection['twitter_db']


buckets = ['travelbucket']

per_page = 50


def new_home(request):
    twitter_profiles    =   []
    collection          =   mongo_db['%s_profile_collection'%('travelbucket')]

    page_num            =   request.GET.get('page', 1)
    followers_count     =   request.GET.get('followers_count', 0)
    friends_count       =   request.GET.get('friends_count', 0)
    follower_oper       =   request.GET.get('follower_operation', '$gt')
    friend_oper         =   request.GET.get('friend_operation', '$gt')
    
    q = '&'.join(['%s=%s'%(k,v) for k,v in request.GET.items() if k!='page'])

    query               =   request.GET.get('query', None)
    print query 
    if query and query!=' Search':
        query_words     =   query.lower().split()
        results         =   collection.find({
                                'followers_count'   :   {follower_oper:int(followers_count)},
                                'friends_count'     :   {friend_oper:int(friends_count)},
                                'keywords'          :   {'$in': query_words}
                            })
    else:
        results         =   collection.find({
                                'followers_count'   :   {follower_oper:int(followers_count)},
                                'friends_count'     :   {friend_oper:int(friends_count)}
                            })

    for m in results[(int(page_num)-1)*per_page : int(page_num)*per_page]:
        twitter_profiles.append(m)
    total_count         =   results.count()
    if int(page_num)*per_page>total_count: next_page_num   =   None
    else: next_page_num   =   int(page_num) + 1
    if int(page_num)==1: prev_page_num   =   None
    else: prev_page_num   =   int(page_num) - 1
    
    return render_to_response('new_home.html', {
                    'twitter_profiles': twitter_profiles, 
                    'buckets': buckets, 
                    'total_count': total_count,
                    'current_page_num': int(page_num),
                    'next_page_num': next_page_num,
                    'prev_page_num': prev_page_num,
                    'q': None if q=='' else q
                })


def home(request):
    buckets = Bucket.objects.all().order_by('-id')
    return render_to_response('home.html', {'buckets': buckets})


@csrf_exempt
def create_bucket(request, bucketid=None):
    bucket = None
    if bucketid:
        bucket = Bucket.objects.get(id=bucketid)
    if request.method=='POST':
        if bucket:
            bucket.name             =   request.POST.get('name')
            bucket.keywords         =   request.POST.get('keywords')
            bucket.status           =   request.POST.get('status', 'CREATED')
            bucket.save()
            seed_profiles           =   [s for s in request.POST.get('seed', '').split(',') if s!='']
            for sp in seed_profiles:
                tw_prof, created    =   TwitterProfile.objects.get_or_create(
                                                username    =   sp, 
                                                defaults    =   {'bucket': bucket, 'seedprofile': True, 'explored': False}
                                            )
        else:
            name        =   request.POST.get('name')
            keywords    =   request.POST.get('keywords')
            status      =   request.POST.get('status', 'CREATED')
            if name and keywords:
                bucket, created     =   Bucket.objects.get_or_create(
                                            name=name, 
                                            keywords=keywords, 
                                            defaults={'status':'CREATED'}
                                        )
                seed_profiles       =   [s for s in request.POST.get('seed', '').split(',') if s!='']
                for sp in seed_profiles:
                    tw_prof, crtd   =   TwitterProfile.objects.get_or_create(
                                                username    =   sp, 
                                                defaults    =   {'bucket': bucket, 'seedprofile': True, 'explored': False}
                                            )
        return HttpResponseRedirect('/')
    else:
        return render_to_response('create_bucket.html', {'bucket': bucket})


def insight(request):
    pagenum     =   request.GET.get('page', 1)
    paginator   =   Paginator(TwitterProfile.objects.all(), 100)
    objectlist  =   paginator.page(pagenum)
    userlist    =   []
    for obj in objectlist:
        try:
            if obj.userdetail:
                user_detail         =   json.loads(obj.userdetail) if obj.userdetail else {'followers_count': 0, 'friends_count': 0}
                if user_detail.get('created_at'):
                    created_at      =   datetime.strptime(user_detail['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
                else:
                    created_at      =   datetime(1,1,1)
                user_followers      =   json.loads(obj.userfollowers) if obj.userfollowers else []
                user_friends        =   json.loads(obj.userfriends) if obj.userfriends else []
                commonfriends_count =   len(set(user_friends).intersection(set(user_followers)))
                userlist.append(
                                {
                                    'username': obj.username, 
                                    'userdetail': user_detail, 
                                    'commonfriends_count': commonfriends_count,
                                    'created_at': created_at
                                    
                                }
                            )
        except Exception, e:
            print e
            print obj.id
    return render_to_response('insight.html', {'userlist': userlist})


