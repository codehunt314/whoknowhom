import twitter, redis, re, random, httplib, urllib, json, time, urllib2
import oauth2 as oauth
from pymongo import Connection

redis_db    =   redis.Redis(host='localhost', port=6379, db=10)
connection  =   Connection()
mongo_db    =   connection['twitter_db']


BUCKET_DICT     =   {
                        'travelbucket':     {
                                                'keywords':     [
                                                                    'traveller', 'travellers', 'traveler', 'travelers', 'travel', 'travelling', 'traveling',
                                                                    'journey', 'trip', 'nomad', 'photographer', 'photography', 'nomadic', 'adventure', 'travelogues',
                                                                    'vacation', 'wandering', 'wander', 'wanders', 'wanderer', 'wanderers', 'tourism', 'tour', 'nomads', 
                                                                    'nomadics', 'backpacker', 'backpackers', 'backpack', 'backpacking', 'travelholic', 'wildlife', 
                                                                    'adventurer', 'globetrotter', 'globetrotters', 'wild life', 
                                                                ],
                                                'seed_profile': ['vipulshaily']
                                            }
                    }


def is_valid_profile(profile, bucket):
    keywords                =   BUCKET_DICT[bucket]['keywords']
    valid_profile           =   False
    profile_name            =   profile.get('screen_name')
    if profile_name.find('travel')!=-1:
        valid_profile       =   True
    profile_desc            =   profile.get('description', None)
    if profile_desc:
        profile_desc_lower  =   profile_desc.lower()
        for kw in keywords:
            if profile_desc_lower.find(kw)!=-1:
                valid_profile   =   True
    return valid_profile


class TwitterCrawl:
    def __init__(self):
        self.TWITTER_ACCESS_TOKEN       =   "22993288-ZpHGOa05gVO0fEYoDKMwAhrjqcQjHRgAuOv4IOLzS"
        self.TWITTER_ACCESS_SECRET      =   "fvQtLq75iXq5eWzCz6UMJuih7gLznM24xl4Sb2Nw"
        self.TWITTER_CONSUMER_KEY       =   "22lK0uGUXJf29VmQZrNFOA"
        self.TWITTER_CONSUMER_SECRET    =   "dIoTefWuxwqJzDwWTdz7c2iirFPxysZTk75kxqYnRY"
        self.twitter                    =   twitter.Api(
                                                consumer_key        =   self.TWITTER_CONSUMER_KEY,
                                                consumer_secret     =   self.TWITTER_CONSUMER_SECRET,
                                                access_token_key    =   self.TWITTER_ACCESS_TOKEN,
                                                access_token_secret =   self.TWITTER_ACCESS_SECRET
                                            )


    def fetch_friends(self, screen_name=None, user_id=None, cursor=-1, bucket='travelbucket'):
        """
            Fetching profile of friends (200 at a time), until fetch profiles of all the friends.
            Might take longer in case user has large number of friends
        """
        url                 =   '%s/friends/list.json' % self.twitter.base_url
        parameters          =   {}
        redis_friend_key    =   '%s$%s$friends'%(bucket, screen_name)
        if user_id is not None:
              parameters['user_id']         =   user_id
        if screen_name is not None:
              parameters['screen_name']     =   screen_name
        parameters['count']                 =   200
        parameters['skip_status']           =   True
        parameters['include_user_entities'] =   True

        while True:
            self.twitter.GoToSleep('/friends/list')
            parameters['cursor']    =   cursor
            json_response           =   self.twitter._RequestUrl(url, 'GET', data=parameters)
            data                    =   self.twitter._ParseAndCheckTwitter(json_response.content)
            for x in data['users']:
                """
                    - add friends to user's friend-list
                    - filter profile based on bucket's keywords
                    - add filtered profile to queue (for crawling)
                """
                redis_db.sadd(redis_friend_key, twitter.User.NewFromJsonDict(x))
                if is_valid_profile(x, bucket):
                    collection  =   mongo_db['%s_profile_collection'%(bucket)]
                    collection.insert(x)
                    redis_db.sadd('%s$userqueue'%(bucket), x['screen_name'])
            if 'next_cursor' in data:
                if data['next_cursor'] == 0 or data['next_cursor'] == data['previous_cursor']:
                    break
                else:
                    cursor = data['next_cursor']
            else:
                break


    def fetch_follower_ids(self, screen_name=None, user_id=None, cursor=-1, bucket='travelbucket'):
        url                     =   '%s/followers/ids.json' % self.twitter.base_url
        parameters              =   {}
        redis_followersid_key   =   '%s$%s$followersid'%(bucket, screen_name)
        if user_id is not None:
              parameters['user_id']         =   user_id
        if screen_name is not None:
              parameters['screen_name']     =   screen_name
        parameters['count']                 =   5000
        while True:
            self.twitter.GoToSleep('/followers/ids')
            parameters['cursor']    =   cursor
            json_response           =   self.twitter._RequestUrl(url, 'GET', data=parameters)
            data                    =   self.twitter._ParseAndCheckTwitter(json_response.content)
            for x in data['ids']:
                redis_db.sadd(redis_followersid_key, x)
            if 'next_cursor' in data:
                if data['next_cursor'] == 0 or data['next_cursor'] == data['previous_cursor']:
                    break
                else:
                    cursor = data['next_cursor']
            else:
                break


def start_crawling(bucket='travelbucket'):
    tw_crawl    =   TwitterCrawl()
    for sp in BUCKET_DICT[bucket]['seed_profile']:
        tw_crawl.fetch_friends(screen_name=sp)
    for screen_name in redis_db.smembers('%s$userqueue'%(bucket)):
        if not redis_db.exists('%s$%s$friends'%(bucket, screen_name)):
            tw_crawl.fetch_friends(screen_name=screen_name, bucket=bucket)

def fetch_followers_ids(bucket='travelbucket'):
    tw_crawl    =   TwitterCrawl()
    for screen_name in redis_db.smembers('%s$userqueue'%(bucket)):
        if redis_db.exists('%s$%s$friends'%(bucket, screen_name)):
            tw_crawl.fetch_follower_ids(screen_name=screen_name, bucket=bucket)


