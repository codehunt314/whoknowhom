import re, random, httplib, urllib, json, time, urllib2
import oauth2 as oauth

TWITTER_ACCESS_TOKEN    =   "22993288-ZpHGOa05gVO0fEYoDKMwAhrjqcQjHRgAuOv4IOLzS"
TWITTER_ACCESS_SECRET   =   "fvQtLq75iXq5eWzCz6UMJuih7gLznM24xl4Sb2Nw"
TWITTER_CONSUMER_KEY    =   "22lK0uGUXJf29VmQZrNFOA"
TWITTER_CONSUMER_SECRET =   "dIoTefWuxwqJzDwWTdz7c2iirFPxysZTk75kxqYnRY"

REQUEST_COUNT = 0
told = None

from mainapp.models import *

def internet_on():
    try:
        response=urllib2.urlopen('http://74.125.228.100',timeout=1)
        return True
    except urllib2.URLError as err: pass
    return False


def _get_request(url, parameters=None, http_method='GET'):
    while not internet_on():
        print 'network in down, will try in 10 sec'
        time.sleep(10)
    global REQUEST_COUNT
    REQUEST_COUNT += 1
    if REQUEST_COUNT==1:
        global told
        told  = time.time()
    if REQUEST_COUNT==15:
        REQUEST_COUNT = 0
        timedelta = 900 - (time.time() - told)
        if timedelta>0:
            print "twitter request limit(15 request/min) reached, wating for %s second"%(timedelta)
            time.sleep(timedelta+60)
    connection              =   httplib.HTTPSConnection("api.twitter.com")
    consumer                =   oauth.Consumer(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    signature_method        =   oauth.SignatureMethod_HMAC_SHA1()
    token                   =   oauth.Token.from_string("oauth_token=%s&oauth_token_secret=%s"%(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_SECRET))
    oauth_request           =   oauth.Request.from_consumer_and_token(consumer, token=token, http_method=http_method, http_url=url, parameters=parameters)
    oauth_request.sign_request(signature_method, consumer, token)
    connection.request(oauth_request.method, oauth_request.to_url())
    return json.loads(connection.getresponse().read())


def _get_friends(screen_name, cursor='-1', http_method='GET'):
    url = 'https://api.twitter.com/1.1/friends/ids.json'
    param = {'screen_name': screen_name, 'count': 5000, 'cursor': cursor}
    data = _get_request(url, parameters=param, http_method=http_method)
    if data.get('next_cursor'):
        new_data = _get_friends(screen_name, cursor=data.get('next_cursor'))
        new_data['ids'].extend(data['ids'])
        return new_data
    return data


def _get_followers(screen_name, cursor='-1', http_method='GET'):
    url = 'https://api.twitter.com/1.1/followers/ids.json'
    param = {'screen_name': screen_name, 'count': 5000, 'cursor': cursor}
    data = _get_request(url, parameters=param, http_method=http_method)
    if data.get('next_cursor'):
        new_data = _get_followers(screen_name, cursor=data.get('next_cursor'))
        new_data['ids'].extend(data['ids'])
        return new_data
    return data



def _chunks(l, n=100):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def _get_profile(userids):
    url = "https://api.twitter.com/1.1/users/lookup.json"
    param = {'user_id': userids}
    userid_chunks = _chunks(userids, 100)
    data = []
    for uids in userid_chunks:
        uids_str = ', '.join([str(i) for i in uids])
        param = {'user_id': uids_str}
        data.extend(_get_request(url, parameters=param, http_method='GET'))
    return data


#get_twitter_detail('sanjaynath', None)
#get_twitter_detail('vipulshaily', None)
#get_twitter_detail('nitinhayaran', None)
#get_twitter_detail('rohitmuppidi', None)
#get_twitter_detail('anand_raj', None)
#get_twitter_detail('vishalgondal', None)



def _explore_queue():
    for prof in TwitterProfile.objects.filter(seedprofile=True, explored=False):
        print 'exploring @%s'%(prof.username.strip())
        friends             =   _get_friends(prof.username.strip())['ids']
        followers           =   _get_followers(prof.username.strip())['ids']
        prof.userfriends    =   json.dumps(friends)
        prof.userfollowers  =   json.dumps(followers)
        prof.save()
        print '\tfriends   of @%s (%s)'%(prof.username, len(friends))
        print '\tfollowers of @%s (%s)'%(prof.username, len(followers))
        friends.extend(followers)
        unique_userids      =   list(set(friends))
        for userprof in _get_profile(unique_userids):
            tf, created = TwitterProfile.objects.get_or_create(
                                username = userprof['screen_name'].strip(), 
                                defaults = {
                                    'userid': userprof['id_str'], 
                                    'userdetail': userprof, 
                                    'bucket': prof.bucket, 
                                    'seedprofile': True, 
                                    'explored': False
                                    }
                                )
            if not created:
                if not tf.userdetail:
                    tf.userdetail = userprof
                    tf.save()
        prof.explored       =   True
        prof.save()
    _explore_queue()

"""
for prof in TwitterProfile.objects.filter(seedprofile=True, explored=True):
    if prof.userfriends and prof.userfollowers:
        print prof.username, len(json.loads(prof.userfriends)), '\t', len(json.loads(prof.userfollowers))
    print len(json.loads(prof.userfriends))
    print len(json.loads(prof.userfollowers))

"""

