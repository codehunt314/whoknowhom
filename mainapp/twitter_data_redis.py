import twitter, redis, re, random, httplib, urllib, json, time, urllib2
import oauth2 as oauth

redis_db    =   redis.Redis(host='localhost', port=6379, db=1)

BUCKET_DICT = {
            'travelbucket': [
                'traveller', 'travellers', 'traveler', 'travelers', 'travel', 'travelling', 'traveling',
                'journey', 'trip', 'nomad', 'photographer', 'photography', 'nomadic', 'adventure', 'travelogues',
                'vacation', 'wandering', 'wander', 'wanders', 'wanderer', 'wanderers', 'tourism', 'tour', 'nomads', 
                'nomadics', 'backpacker', 'backpackers', 'backpack', 'backpacking', 'travelholic', 'wildlife', 
                'adventurer', 'globetrotter', 'globetrotters', 'wild life', 
            ]
        }

def twitter_login():
    TWITTER_ACCESS_TOKEN    =   "22993288-ZpHGOa05gVO0fEYoDKMwAhrjqcQjHRgAuOv4IOLzS"
    TWITTER_ACCESS_SECRET   =   "fvQtLq75iXq5eWzCz6UMJuih7gLznM24xl4Sb2Nw"
    TWITTER_CONSUMER_KEY    =   "22lK0uGUXJf29VmQZrNFOA"
    TWITTER_CONSUMER_SECRET =   "dIoTefWuxwqJzDwWTdz7c2iirFPxysZTk75kxqYnRY"
    if TWITTER_ACCESS_SECRET and TWITTER_ACCESS_SECRET:
        return  twitter.Api(
                    consumer_key        =   TWITTER_CONSUMER_KEY,
                    consumer_secret     =   TWITTER_CONSUMER_SECRET,
                    access_token_key    =   TWITTER_ACCESS_TOKEN,
                    access_token_secret =   TWITTER_ACCESS_SECRET
                )



api = twitter_login()


def is_valid_profile(profile, bucket):
    keywords        =   BUCKET_DICT[bucket]
    valid_profile   =   False
    profile_name        =   profile.get('screen_name')
    if profile_name.find('travel')!=-1:
        valid_profile   =   True
    profile_desc            =   profile.get('description', None)
    if profile_desc:
        profile_desc_lower  =   profile_desc.lower()
        for kw in keywords:
            if profile_desc_lower.find(kw)!=-1:
                valid_profile   =   True
    return valid_profile


def fetch_and_save_friends(screen_name=None, bucket='travelbucket'):
    if screen_name:
        # if not redis_db.exists('%s$%s$profile'%(bucket, screen_name)):
        #     user_profile                =   api.GetUser(screen_name=screen_name, user_id=user_id)
        #     print 'user profile fetched for \"%s\"'%(screen_name)
        #     user_profile_added_status   =   redis_db.sadd('%s$%s$profile'%(bucket, screen_name), user_profile)
        #     print 'user profile saved for \"%s\"'%(screen_name)
        # else:
        #     print 'Profile already exist for \"%s\"'%(screen_name)
        # user_profile = json.loads(list(redis_db.smembers('%s$%s$profile'%(bucket, screen_name)))[0])
        # if user_profile.get('friends_count', 0)>3000:
        #     return False
        if not redis_db.exists('%s$%s$friends'%(bucket, screen_name)):
            print 'fetching friends of \"%s\"'%(screen_name)
            friends                     =   api.GetFriends(screen_name=screen_name, count=200)
            user_friends_added_status   =   [redis_db.sadd('%s$%s$friends'%(bucket, screen_name), f) for f in friends]
            print 'user friends saved for \"%s\"'%(screen_name)
            return [json.loads(m) for m in redis_db.smembers('%s$%s$friends'%(bucket, screen_name))]
        else:
            print 'Friends already exist for \"%s\"'%(screen_name)
            return   [json.loads(m) for m in redis_db.smembers('%s$%s$friends'%(bucket, screen_name))]
        print '\n'
    else:
        print 'screen_name and user_id both are None :('


def fetch_friends(screen_name=None, user_id=None, cursor=-1, bucket='travelbucket'):
    redis_friend_key = '%s$%s$friends'%(bucket, screen_name)
    if redis_db.exists(redis_friend_key):
        return redis_db.smembers(redis_friend_key)
    url         =   '%s/friends/list.json' % api.base_url
    parameters  =   {}
    if user_id is not None:
          parameters['user_id']         =   user_id
    if screen_name is not None:
          parameters['screen_name']     =   screen_name
    parameters['count']                 =   200
    parameters['skip_status']           =   True
    parameters['include_user_entities'] =   True

    while True:
        api.GoToSleep('/friends/list')
        parameters['cursor']    =   cursor
        json_response           =   api._RequestUrl(url, 'GET', data=parameters)
        data                    =   api._ParseAndCheckTwitter(json_response.content)
        for x in data['users']:
            redis_db.sadd(redis_friend_key, twitter.User.NewFromJsonDict(x))
            if is_valid_profile(x, bucket):
                add_in_queue(x['screen_name'], bucket)
                redis_db.set('%s$%s$profile'%(bucket, x['screen_name']), json.dumps(x))
        if 'next_cursor' in data:
            if data['next_cursor'] == 0 or data['next_cursor'] == data['previous_cursor']:
                redis_db.hset('%s$cursor'%(bucket), screen_name, 'done')
                break
            else:
                cursor = data['next_cursor']
                redis_db.hset('%s$cursor'%(bucket), screen_name, cursor)
        else:
            redis_db.hset('%s$cursor'%(bucket), screen_name, 'done')
            break
    redis_db.sadd('%s$userqueueprocessed'%(bucket), screen_name)
    return redis_db.smembers(redis_friend_key)

def fetch_followers(screen_name=None, user_id=None, cursor=-1, bucket='travelbucket'):
    redis_followers_key = '%s$%s$followers'%(bucket, screen_name)
    if redis_db.exists(redis_followers_key):
        return redis_db.smembers(redis_followers_key)
    url         =   '%s/followers/list.json' % api.base_url
    parameters  =   {}
    if user_id is not None:
          parameters['user_id']         =   user_id
    if screen_name is not None:
          parameters['screen_name']     =   screen_name
    parameters['count']                 =   200
    parameters['skip_status']           =   True
    parameters['include_user_entities'] =   True

    while True:
        api.GoToSleep('/followers/list')
        parameters['cursor']    =   cursor
        json_response           =   api._RequestUrl(url, 'GET', data=parameters)
        data                    =   api._ParseAndCheckTwitter(json_response.content)
        for x in data['users']:
            redis_db.sadd(redis_followers_key, twitter.User.NewFromJsonDict(x))
            if is_valid_profile(x, bucket):
                add_in_queue(x['screen_name'], bucket)
                redis_db.set('%s$%s$profile'%(bucket, x['screen_name']), json.dumps(x))
        if 'next_cursor' in data:
            if data['next_cursor'] == 0 or data['next_cursor'] == data['previous_cursor']:
                break
            else:
                redis_db.hset('%s$cursor'%(bucket), screen_name, cursor)
        else:
            break
    return redis_db.smembers(redis_followers_key)

def fetch_followers_id(screen_name=None, user_id=None, cursor=-1, bucket='travelbucket'):
    redis_followersid_key = '%s$%s$followersid'%(bucket, screen_name)
    if redis_db.exists(redis_followersid_key):
        return redis_db.smembers(redis_followersid_key)
    url         =   '%s/followers/ids.json' % api.base_url
    parameters  =   {}
    if user_id is not None:
          parameters['user_id']         =   user_id
    if screen_name is not None:
          parameters['screen_name']     =   screen_name
    parameters['count']                 =   5000
    while True:
        api.GoToSleep('/followers/ids')
        parameters['cursor']    =   cursor
        json_response           =   api._RequestUrl(url, 'GET', data=parameters)
        data                    =   api._ParseAndCheckTwitter(json_response.content)
        for x in data['ids']:
            redis_db.sadd(redis_followersid_key, x)
        if 'next_cursor' in data:
            if data['next_cursor'] == 0 or data['next_cursor'] == data['previous_cursor']:
                break
            else:
                cursor = data['next_cursor']
        else:
            break
    return redis_db.smembers(redis_followersid_key)


def friends_crawler(bucket):
    for screen_name in redis_db.smembers('%s$userqueue'%(bucket)):
        fetch_friends(screen_name, bucket)
        time.sleep(60)


def add_in_queue(screen_name, bucket):
    # print 'adding \"%s\"" into queue' % (screen_name)
    if not redis_db.sismember('%s$userqueue'%(bucket), screen_name):
        redis_db.sadd('%s$userqueue'%(bucket), screen_name)
        #redis_db.rpush('%s$profiles'%(bucket), screen_name)

def update_seed(bucket):
    screen_name_seeds = ['vipulshaily', 'devilonwheeels']
    for sn in screen_name_seeds: add_in_queue(sn, bucket)


def rule_friend_following_back(screen_name, bucket):
    friend_key      =   '%s$%s$friends'%(bucket, screen_name)
    followers_id    =   fetch_followers_id(screen_name=screen_name, bucket=bucket)
    friends         =   fetch_friends(screen_name=screen_name, bucket=bucket)
    friends_who_are_following = []
    for f in friends:
        if f['id'] in followers_id: friends_who_are_following.append(f)
    return len(friends_who_are_following)

"""
def rule_potential_social_impact(screen_name, bucket):
    #follower count of followers
    friends                 =   fetch_followers(screen_name=screen_name, bucket=bucket)
    return sum([int(f['followers_count']) for f in friends])
"""

def rule_find_clique(screen_name, bucket):
    pass

def rule_x(screen_name, bucket):
    pass


"""
from pymongo import Connection
connection = Connection()
db = connection['%s_db'%(bucket)]
#collection = db['%s_collection'%(bucket)]
db.entry.insert(d)
db.entry.count()
for i in db.entry.find({'followers_count':1377}): i
for i in db.entry.find({'followers_count':{"$lt":100}}): i
for i in db.entry.find({'followers_count':{"$gt":100}}): i
db.entry.insert([1,2])


"""
