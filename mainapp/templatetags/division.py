from datetime import datetime, timedelta
from django.utils.timesince import timesince
from django import template
import calendar

register = template.Library()

@register.filter
def divide(value, arg):
    if arg==0: return 0.0
    return float(value) / float(arg)

@register.filter
def formatdate(value):
    creation_date = datetime.strptime(value, '%a %b %d %H:%M:%S +0000 %Y')
    return '%s, %s'%(calendar.month_name[creation_date.month][:3], creation_date.year)

# def duration(value, arg):
#     difference = datetime.now() - datetime.strptime(value, '%a %b %d %H:%M:%S +0000 %Y')
#     if difference <= timedelta(days=1):
#         return 'Today'
#     return '%(time)s ago' % {'time': timesince(value).split(', ')[0]}


