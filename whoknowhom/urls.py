from django.conf.urls import patterns, include, url
import settings

urlpatterns = patterns('',
    url(r'^$', 'mainapp.views.new_home', name='new_home'),
    url(r'^home/$', 'mainapp.views.home', name='home'),
    url(r'^createbucket/$', 'mainapp.views.create_bucket', name='create_bucket'),
    url(r'^bucket/(?P<bucketid>\d+)/edit/$', 'mainapp.views.create_bucket', name='create_bucket'),
    url(r'^insight/$', 'mainapp.views.insight', name='insight'),
)


"""
url(r'^$', 'mainapp.views.home', name='home'),
    url(r'^peoplesearch/$', 'mainapp.views.peoplesearch', name='peoplesearch'),
    url(r'^recipe/$', 'recipe.views.recipehome', name='recipehome'),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
"""
